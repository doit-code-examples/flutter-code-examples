import 'package:flutter/material.dart';
import 'package:mcetc/models/model.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:mcetc/views/login_screen.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

//requires later than ios8 / api16
void main() => runApp(
      ChangeNotifierProvider(
        create: (context) => Model(),
        child: MyApp(),
      ),
    );

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MCETC',
      theme: ThemeData(
        primaryColor: AppColors.body,
        primaryTextTheme: GoogleFonts.patrickHandTextTheme(
          Theme.of(context).textTheme,
        ),
        accentTextTheme: GoogleFonts.patrickHandTextTheme(
          Theme.of(context).textTheme,
        ),
        textTheme: GoogleFonts.patrickHandTextTheme(
          Theme.of(context).textTheme,
        ),
        appBarTheme: AppBarTheme(
          color: Colors.white,
          elevation: 0.0,
        ),
      ),
      home: LoginScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
