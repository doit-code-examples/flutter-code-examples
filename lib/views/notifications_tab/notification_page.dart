import 'package:flutter/material.dart';
import 'package:mcetc/models/model.dart';
import 'package:mcetc/services/api_service.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:mcetc/views/notifications_tab/widgets/notification_image.dart';
import 'package:mcetc/views/transition_animations/slide_route.dart';
import 'package:mcetc/views/widgets/mi_app_bar.dart';
import 'package:provider/provider.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage(
      {Key key, this.imageProvider, this.title, this.text, this.id})
      : super(key: key);

  final String id;
  final ImageProvider imageProvider;
  final String title;
  final String text;

  @override
  Widget build(BuildContext context) {
    //on build stage setting callback that after build
    //this notification should be marked as viewed (visualy in list and on server side)
    _markNotificationAsViewed(context);

    //Constraining image to 40% of screen length
    final image = Container(
      width: MediaQuery.of(context).size.height * 0.4,
      margin: EdgeInsets.only(top: 10),
      child: Image(image: imageProvider),
    );

    //title widget
    final titleW = Container(
      width: double.infinity,
      child: Text(
        title,
        style: TextStyle(
          color: AppColors.body,
          fontSize: 20,
        ),
      ),
    );

    //text widget
    final textW = Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 10),
      child: Text(
        text,
        style: TextStyle(
          color: AppColors.body.withOpacity(0.8),
          fontSize: 16,
        ),
      ),
    );

    return Scaffold(
      appBar: MiAppBar(
        title: "Notifications",
      ),
      body: GestureDetector(
        //detecting horizontal drags in order to navigate to other notifications in list
        onHorizontalDragEnd: (DragEndDetails details) =>
            _onHorizontalDrag(context, details),
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children:
                  //widget elements from which page content consists
                  <Widget>[
                image,
                titleW,
                textW,
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onHorizontalDrag(BuildContext context, DragEndDetails details) {
    // user have just tapped on screen (no dragging)
    if (details.primaryVelocity == 0) return; //ignoring this

    //gets model data
    var model = Provider.of<Model>(context, listen: false);

    //user swipes from right to left
    if (details.primaryVelocity.compareTo(0) == -1) {
      //navigates to next notification page
      _toNext(context, model);
    }
    //user swipes from left to right
    else {
      //navigates to previous notification page
      _toPrevious(context, model);
    }
  }

  //gets information of next in the list notification
  //and passes it to navigation method
  void _toNext(BuildContext context, model) {
    var notification = model.getNextNotification(id);

    //if next notification is not exists
    //because this one is the last one - ignoring request
    if (notification != null)
      navigateToNotification(context, notification, true);
  }

  //gets information of next in the list notification
  //and passes it to navigation method
  void _toPrevious(BuildContext context, model) {
    var notification = model.getPreviousNotification(id);

    //if previous notification is not exists
    //because this one is the first one - ignoring request
    if (notification != null)
      navigateToNotification(context, notification, false);
  }

  //using specific transition animation depends on navigation to next or previous notifacation
  void navigateToNotification(BuildContext context, notification, bool right) {
    Navigator.pushReplacement(
      context,
      SlideRoute(
        right: right,
        page: NotificationPage(
          id: notification['id'],
          imageProvider: getImageProviderFromNotification(notification),
          title: notification['notification']['title'],
          text: notification['notification']['text'],
        ),
      ),
    );
  }

  //After build marking notifications as viewed if it wasn't viewed before
  void _markNotificationAsViewed(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      var model = Provider.of<Model>(context, listen: false);
      if (model.notifications.isNotEmpty) {
        final List<String> notificationInList = [id];

        final bool result = model.assignViewedNotification(id);

        if (notificationInList.isNotEmpty && result) {
          ApiService.markNotificationsAsViewed(
                  model.user['id'], notificationInList)
              .then((response) => print(response.body));
        }
      }
    });
  }
}
