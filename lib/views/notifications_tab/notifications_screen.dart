import 'package:flutter/material.dart';
import 'package:mcetc/models/model.dart';
import 'package:mcetc/services/api_service.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:mcetc/views/notifications_tab/notification_page.dart';
import 'package:mcetc/views/notifications_tab/widgets/notification_image.dart';
import 'package:mcetc/views/notifications_tab/widgets/notification_list_item.dart';
import 'package:mcetc/views/widgets/loading_indicator.dart';
import 'dart:convert' as convert;
import 'package:provider/provider.dart';
import 'package:date_format/date_format.dart';

class NotificationsScreen extends StatefulWidget {
  NotificationsScreen({Key key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  var _model;
  bool _isLoading = false;
  ScrollController _controller;

  @override
  void initState() {
    super.initState();

    //initializing scroll controller and adding listener
    //as a callback to certain action
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    //getting access to data in model
    _model = Provider.of<Model>(context);
  }

  @override
  Widget build(BuildContext context) {
    //building appBar as separate widget
    final appBar = AppBar(
      elevation: 0,
      title: Text(
        'Notifications',
        style: TextStyle(
          fontSize: 24,
          color: AppColors.body,
        ),
      ),
      centerTitle: true,
      automaticallyImplyLeading: false,
    );

    //returns scaffold page with given background, appBar, and listView
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: appBar,
      body: _listViewBuilder(),
    );
  }

  //returns a listView
  //before loading a layout checking if notifications have been loaded in this session
  //thus checking if array of notifications is empty
  //if yes - uploads data from Api and saves it to model
  //if no - takes saved data from model
  Widget _listViewBuilder() {
    //calling API marking function to mark as viewed all notification of type 1 which are currently on the screen
    //this call will be made only when layout build is finished
    _markCommonNotifications();

    return _model.notifications.isEmpty
        ? FutureBuilder(
            future: ApiService.getUserNotifications(_model.user['id']),
            builder: (context, snapshot) {
              //when all data gethered
              if (snapshot.hasData) {
                //converting json in body of response to map
                var data = convert.jsonDecode(snapshot.data.body);

                //saving uploaded data to model
                _model.addNotifications(data['notifications']);

                //saving data about status of data
                _model.notificationsMeta = data['meta'];

                return _listView();
              }

              //if there is an error occurs afrer sending a request
              //return text of the error
              else if (snapshot.hasError) {
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              }

              //while waiting for data to load
              //loading indicator is shown
              return Center(
                child: LoadingIndicator(),
              );
            })
        : _listView();
  }

  Widget _listView() {
    //returns a listView depending on uploaded data before
    return ListView.builder(
      //assignment of the controller initialized on initState phase
      //to control behaviour of the listView
      controller: _controller,

      //number of items assignment
      //assigning on 1 more than exist for loader to show
      //when data need to be uploaded once more
      //pagination trick
      itemCount: _model.notifications.length + 1,

      //returns items based on data
      itemBuilder: (context, index) {
        //shows indicator when comes to last item of the list
        //shows only when there is data exists to load
        if (index == _model.notifications.length)
          return LoadingIndicator(isLoading: _isLoading);

        //common item
        return _cardTile(index);
      },
    );
  }

  //takes all needed data for list tile based on index
  //and calculates it
  Widget _cardTile(int index) {
    final notificationInfo = _model.notifications[index]['notification'];

    //notification title
    String title = notificationInfo['title'] ?? 'Title';

    //based on type of notification returns image respectively
    ImageProvider image = notificationImage(notificationInfo['type']);

    //notification text
    String text = notificationInfo['text'] ?? 'Text';

    //parsing notification sentAt date
    DateTime date = DateTime.parse(
      _model.notifications[index]['sentAt'],
    );

    //parsing date to string
    //in case of yesterday or today dates returns it by word
    String dateText = dateString(date);

    //if data is viewed before or it is more than 7 days when it was sent
    //represent it as an old one (false)
    //elsewhise - new (true)
    bool isNew = _model.notifications[index]['viewedAt'] == null &&
        calculateDifference(date) >= -7;

    //returning custom list item
    return NotificationListItem(
      thumbnail: Image(image: image), //notification type image
      title: title,
      text: text,
      isNew: isNew,
      date: dateText,
      onTap: () => navigateToNotification(_model.notifications[index]),
      onRemove: () => removeNotification(_model.notifications[index]['id']),
    );
  }

  //scroll listener detects if user scolled to maximal extent
  //in this case uploades more data (pagination)
  void _scrollListener() async {
    if (_controller.position.pixels == _controller.position.maxScrollExtent)
      _uploadData();
  }

  //uploads more data if that exists
  Future _uploadData() async {
    //getting number of already loaded notifications
    int length = _model.notifications.length;

    //from meta checking if there is more notifications exists
    //on server than user has already loaded
    //checking if loading process has not yet started
    if (_model.notificationsMeta['total'] > length && !_isLoading) {
      //rebuilding layout
      //showing that loading started due to loading indicator
      setState(() => _isLoading = true);

      //caclulating a page needed to load from Api
      //depends on current number of notifications
      //and how many should be shown per page (per one load)
      int pageToLoad =
          (length / _model.notificationsMeta['perPage']).floor() + 1;

      //sending request to get a page of notifications loaded and receiving response
      var response = await ApiService.getUserNotifications(_model.user['id'],
          page: pageToLoad);

      //based on response
      //if data successfully loaded
      if (response.statusCode == 200) {
        //converting json to map
        var data = convert.jsonDecode(response.body);

        //adding new notifications to already existing one
        _model.updateNotifications(data['notifications']);
      }

      //rebuilding layout as our list of notifications changed
      //and load has finished
      setState(() => _isLoading = false);
    }
  }

  //returns date in a special string format
  //also depends on difference with current date
  String dateString(DateTime date) {
    switch (calculateDifference(date)) {
      case -1:
        return 'Yesterday';

      case 0:
        return 'Today';

      default:
        return formatDate(
          date,
          [M, ' ', dd],
        );
    }
  }

  // Returns the difference (in full days) between the provided date and today.
  int calculateDifference(DateTime date) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day)
        .difference(DateTime(now.year, now.month, now.day))
        .inDays;
  }

  //send a request to Api to mark all notifications of type 1 as viewed
  //for next log in
  void _markCommonNotifications() {
    //calls in only after build
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_model.notifications.isNotEmpty) {
        List<String> commonNotificationsIds = List<String>.from(_model
            .notifications
            .where((notification) =>
                notification['notification']['type'] == 1 &&
                notification['viewedAt'] == null)
            .map((notification) => notification['id']));

        if (commonNotificationsIds.isNotEmpty) {
          ApiService.markNotificationsAsViewed(
                  _model.user['id'], commonNotificationsIds)
              .then((response) => print(response.statusCode));
        }
      }
    });
  }

  //navigates screen to selected notification page for details
  //providing all necessary data
  void navigateToNotification(notification) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NotificationPage(
          id: notification['id'],
          imageProvider: getImageProviderFromNotification(notification),
          title: notification['notification']['title'],
          text: notification['notification']['text'],
        ),
      ),
    );
  }

  //removes notification from user profile
  //visually and on server side for ongoing work
  void removeNotification(String id) => _model.removeNotification(id);
}
