import 'package:flutter/material.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:mcetc/views/notifications_tab/widgets/alert_dialog.dart';
import 'package:mcetc/views/notifications_tab/widgets/dismissible_background.dart';
import 'package:mcetc/views/widgets/text_adjuster.dart';

class NotificationListItem extends StatelessWidget {
  const NotificationListItem(
      {Key key,
      this.thumbnail,
      this.title,
      this.text,
      this.isNew,
      this.date,
      this.onTap,
      this.onRemove})
      : super(key: key);

  final Widget thumbnail;
  final String title;
  final String text;
  final bool isNew;
  final String date;
  final Function onTap;
  final Function onRemove;

  @override
  Widget build(BuildContext context) {
    //adjusting place for type image and showing it on notification tile
    final image = Expanded(
      flex: 3,
      child: thumbnail,
    );

    //building title widget
    final titleW = Container(
      margin: EdgeInsets.only(bottom: 3),
      child: Text(
        title,
        maxLines: 1,
        style: TextStyle(
          color: AppColors.body,
          fontSize: 18,
        ),
      ),
    );

    //building subtitle widget
    final subtitleW = Container(
      margin: EdgeInsets.only(bottom: 10),
      child: TextAdjuster(
        text: text,
        symbolsNum: 60,
        maxLines: 2,
        style: TextStyle(
          color: AppColors.body.withOpacity(0.7),
          fontSize: 16,
        ),
      ),
    );

    //building widget that getting together titile and subtitle
    // on the center of notification tile
    final base = Expanded(
      flex: 9,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 5.0,
          horizontal: 5.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            titleW,
            subtitleW,
          ],
        ),
      ),
    );

    //widget which represents new indicator if needed
    final isNewIndicator = Container(
      margin: EdgeInsets.only(right: 10),
      child: Text(
        isNew ? 'NEW' : '',
        style: TextStyle(
          color: AppColors.body,
          fontSize: 16,
        ),
      ),
    );

    //widget of right expand icon
    final navigateIcon = Icon(
      Icons.keyboard_arrow_right,
      color: AppColors.body,
    );

    //widget represents date
    final dateW = Container(
      margin: EdgeInsets.only(right: 10),
      child: Text(
        date,
        style: TextStyle(
          color: AppColors.body.withOpacity(0.5),
          fontSize: 16,
        ),
      ),
    );

    //in thus widget all three trailing components are listed in row
    final trailing = Container(
      margin: EdgeInsets.only(right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          isNewIndicator,
          navigateIcon,
          dateW,
        ],
      ),
    );

    //returns list tile covered in gesture detector for rap detection
    return GestureDetector(
      onTap: () => onTap(),

      //giving tile a form of a card
      child: Card(
        margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        //covering all content in dismissble widget
        //which allows to make slide to delete action in this situation
        child: Dismissible(
          direction: DismissDirection.endToStart,
          dismissThresholds: {DismissDirection.endToStart: 0.3},
          background: DismissibleBackground(),
          key: UniqueKey(),
          confirmDismiss: (DismissDirection direction) async =>
              deleteConfirmation(context, title),
          onDismissed: (DismissDirection direction) => onRemove(),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              //all major elements of the tile
              children: <Widget>[
                image,
                base,
                trailing,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
