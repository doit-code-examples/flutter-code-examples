import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//calls pop up window and returns bool
//depending on answer
Future<bool> deleteConfirmation(
    BuildContext context, String notificationTitle) async {
  return await showDialog<bool>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title:
                Text('Are you sure you want to delete "$notificationTitle"?'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              CupertinoDialogAction(
                child: Text('Delete'),
                isDestructiveAction: true,
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        },
      ) ??
      false; //if window is dismissed by misclick returns false
}
