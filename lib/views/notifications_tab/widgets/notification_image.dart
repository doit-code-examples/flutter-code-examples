import 'package:flutter/material.dart';
import 'package:mcetc/views/widgets/default_image.dart';

//returns notification type image based on type
AssetImage notificationImage(int type) {
  return AssetImage(
      type == 1 ? 'assets/regular_not.jpg' : 'assets/child_not.jpg');
}

//returns notification image for notification page
//if there's no image - returns pre-selected default image
ImageProvider getImageProviderFromNotification(notification) =>
    notification['notification']['image'] == null
        ? defaultImage()
        : NetworkImage(notification['notification']['image']['path']);
