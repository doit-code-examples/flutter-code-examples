import 'package:flutter/material.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:url_launcher/url_launcher.dart';

enum LinkType { policy, registration, contact }

Map<LinkType, String> linkTitle = {
  LinkType.policy: 'MAKEUP POLICY',
  LinkType.registration: 'REGISTRATION',
  LinkType.contact: 'CONTACT US',
};

Map<LinkType, String> linkToFollow = {
  LinkType.policy: 'https://www.google.com/',
  LinkType.registration: 'https://www.google.com/',
  LinkType.contact: 'https://www.google.com/',
};

class InfoScreen extends StatefulWidget {
  InfoScreen({Key key}) : super(key: key);

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Info'),
        centerTitle: true,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/info_background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          margin: EdgeInsets.only(
            bottom: 80,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buttons(),
          ),
        ),
      ),
    );
  }

  List<Widget> _buttons() =>
      LinkType.values.map((type) => _buttonLinkBuilder(type)).toList();

  Widget _buttonLinkBuilder(LinkType linkType) {
    final String text = linkTitle[linkType];
    final String link = linkToFollow[linkType];

    return Container(
      margin: EdgeInsets.symmetric(vertical: 17),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.5,
        child: OutlineButton(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Text(
            text,
            style: TextStyle(
              color: AppColors.main,
              fontSize: 22,
            ),
          ),
          onPressed: () => _launchURL(link),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          borderSide: BorderSide(
            color: AppColors.main,
            width: 1.5,
          ),
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
