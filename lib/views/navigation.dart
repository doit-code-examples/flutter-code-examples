import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mcetc/services/app_colors.dart';
import 'package:mcetc/views/info_tab/info_screen.dart';
import 'package:mcetc/views/music_tab/dashboard_screen.dart';
import 'package:mcetc/views/notifications_tab/notifications_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum TabItem { notifications, music, info }

Map<TabItem, String> tabTitle = {
  TabItem.notifications: 'Notifications',
  TabItem.music: 'Music',
  TabItem.info: 'Info',
};

Map<TabItem, Color> tabColor = {
  TabItem.notifications: AppColors.secondaryPink,
  TabItem.music: AppColors.main,
  TabItem.info: AppColors.secondaryYellow,
};

Map<TabItem, IconData> tabIcons = {
  TabItem.notifications: Icons.notifications,
  TabItem.music: FontAwesomeIcons.music,
  TabItem.info: Icons.info,
};

class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 1;

  final List<Widget> _tabPages = [
    NotificationsScreen(),
    DashboardScreen(),
    InfoScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabPages[_selectedIndex],
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  List<Widget> _items() {
    return TabItem.values.map<Widget>((tab) => _buildTabItem(tab)).toList();
  }

  _buildTabItem(TabItem tabItem) {
    bool selected = tabItem.index == _selectedIndex;

    final icon = tabItem.index == 1
        ? FaIcon(
            tabIcons[tabItem],
            color: selected ? Colors.white : tabColor[tabItem],
          )
        : Icon(
            tabIcons[tabItem],
            color: selected
                ? Colors.white
                : tabItem.index == 0
                    ? tabColor[tabItem].withOpacity(0.6)
                    : tabColor[tabItem],
          );

    final title = Text(
      tabTitle[tabItem],
      style: TextStyle(
        color: selected
            ? Colors.white
            : tabItem.index == 0
                ? tabColor[tabItem].withOpacity(0.6)
                : tabColor[tabItem],
      ),
    );

    return GestureDetector(
      onTap: () => _selectTab(tabItem.index),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
          color:
              selected ? tabColor[tabItem] : tabColor[tabItem].withOpacity(0.1),
        ),
        height: selected ? 65 : 60,
        width: MediaQuery.of(context).size.width / 3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            icon,
            Text(
              tabTitle[tabItem],
              style: TextStyle(
                color: selected
                    ? Colors.white
                    : tabItem.index == 0
                        ? tabColor[tabItem].withOpacity(0.6)
                        : tabColor[tabItem],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _selectTab(int index) {
    setState(() => _selectedIndex = index);
  }

  Widget _bottomNavigationBar() {
    return Container(
      color: AppColors.background,
      height: 65,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Positioned(
            bottom: 0,
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: _items(),
            ),
          ),
        ],
      ),
    );
  }
}
